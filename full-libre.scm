(define-module (full-libre)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system renpy)
  #:use-module (guix build utils)
  #:use-module (guix git-download))

(define-public learntocode-rpg
  (package
    (name "LearnToCodeRPG")
    (version "v1.5-freeze")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/freeCodeCamp/LearnToCodeRPG.git")
              (commit "v1.5-freeze")))
        (file-name (git-file-name name version))
        (sha256 (base32 "0a9pds58yq8ks6wrm5n4p8g73gbxq52yjyjnn0pr7gz65i31kpqz"))))

    (build-system renpy-build-system)
    
    (home-page "https://freecodecamp.itch.io/learn-to-code-rpg")
    (synopsis "In this game, you will teach yourself to code, make friends in the tech industry, and pursue your dream working a developer🎯")
    (description "Learn to Code RPG is a visual novel game developed by freeCodeCamp.org. In this game, you will teach yourself to code, make friends in the tech industry, and pursue your dream of becoming a developer")
    (license license:bsd-3)))
